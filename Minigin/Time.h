#pragma once
#include "Minigin.h"

namespace division
{
	//class Time : public division::Singleton<Time>
	//{
	//	Time();

	//private:
	//	static const Time m_Instance{ Singleton<Time>::GetInstance() };
	//};

	class Time
	{
	public:
		static Time* getInstance();
		float GetDeltaTime() { return m_deltaTime; };
		
		
	private:
		Time() = default;
		~Time() { delete m_instance; };

		void SetDeltaTime(float deltaTime) { m_deltaTime = deltaTime; };
		
		static Time* m_instance;
		float m_deltaTime;

		friend void division::Minigin::Run();
	};
}

