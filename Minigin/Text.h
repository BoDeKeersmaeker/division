#pragma once
#include  "Component.h"
class Text : public Component
{
public:
	Text() = default;

private:
	std::string m_Text;
};

