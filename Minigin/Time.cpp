#include "MiniginPCH.h"
#include "Time.h"

division::Time* division::Time::m_instance{ nullptr };

division::Time* division::Time::getInstance()
{
	if (m_instance == nullptr)
		m_instance = new Time;

	return m_instance;
}